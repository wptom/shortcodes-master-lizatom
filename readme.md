Lizatom shortcodes
==================

Installation
------------

You are installing this script as any other WordPress plugin. The only requirement is installed Shortcodes Master plugin. Lizatom shortcodes plugin is its addon.

1. Go to the Plugins -> Add New -> Upload
2. Upload shortcodes-master-lizatom.zip
3. Activate Shortcodes Master - Lizatom in the Plugins page


Getting started
---------------

The Shortcodes Master brings you option to insert your shortcodes easily with the shortcodes generator.

Shortcodes overview
-------------------

### 3D button

Please note that you have to enter unique ID.

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#animated-3d-buttons)

Example

``` html
[sm_smlts_3dbutton id="unique-lt3dbutton-1" color="green" target="_self" url="http://mysite.com/url/" icon="down_black" animation="2" float="right"]Download now![/sm_smlts_3dbutton]
```

### Animated portfolio

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#filtered-portfolio)

Example

``` html
[sm_smlts_portfolio id="unique-id-of-portfolio-1" animation="2" columns="3" target="_blank" categories="1,2,3,4,18"]
```

Important! Make sure your theme supports [thumbnails](http://codex.wordpress.org/Function_Reference/add_theme_support)

The first you have to create posts with featured images and assign them to the categories you'll use later in portfolio shortcode.
 
![featured image](http://lizatom.com/wp-content/uploads/2013/04/featured-image.jpg)

Go the Posts-> Categories and hover over the name of category. In the bottom of your browser you'll see ID of the category. It's the part tag_ID=xy. Use the IDs for categories attribute of the portfolio shortcode. 

![featured image](http://lizatom.com/wp-content/uploads/2013/04/category-id.jpg)

### CSS3 accordion

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#css3-accordion)

Example shortcode

``` html
[sm_smlts_css3accordions type="lizatom-css3accordion-horizontal" color="#f58e8e" title="F.A.Q."]
[sm_smlts_css3accordion title="Accordion title must be unique1"]<p>test content 1</p>[/sm_smlts_css3accordion]
[sm_smlts_css3accordion title="Accordion title must be unique2"]<p>test content 2</p>[/sm_smlts_css3accordion]
[sm_smlts_css3accordion title="Accordion title must be unique3"]<p>test content 3</p>[/sm_smlts_css3accordion]
[/sm_smlts_css3accordions]
```
Note in the example above that "[sm_smlts_css3accordion]" part must always be wrapped in "[sm_smlts_css3accordions]"

Important! Don't forget that title of each accordion part must be unique.

``` html
[sm_smlts_css3accordion title="This must be unique on the page"]<p>test content 1</p>[/sm_smlts_css3accordion]
``` 

### Service boxes

[Demo Fatcow >](https://lizatom.com/demo/lizatom-shortcodes/#service-boxes-fatcow)
[Demo Primo >](https://lizatom.com/demo/lizatom-shortcodes/#service-boxes-primo)

Example shortcode

``` html
[sm_smlts_fatcow icon="image: card_money" title="Payment gate"]Lorem ipsum dolor sit amet[/sm_smlts_fatcow]
```

Service boxes icons are zipped in the services.zip. You can get them [here](https://lizatom.com/temp/services.zip)

__Use FTP client__

1. Unzip services.zip
2. Upload folder "services" into the wp-content\plugins\shortcodes-master-lizatom\assets\images\
3. That's it.


### Fancy buttons

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#sexy-buttons)

Example shortcode

``` html
[sm_smlts_fancybutton id="unique-ltfancybutton-id-1" size="large" color="black" target="_blank" url="http://mypages.com" icon="image: information"]My site[/sm_smlts_fancybutton]
```

### Unordered lists

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#unordered-lists)

Example shortcode

``` html
[sm_smlts_list icon="image: tick"]
<li>one</li>
<li>two</li>
<li>three</li>
[/sm_smlts_list]
```

### Tooltips

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#tooltips)

Vimeo video example

``` html
[sm_smlts_tooltip color="light-blue" tooltiphead="Vimeo video" text="Example"]<iframe src="//player.vimeo.com/video/114963142?byline=0&portrait=0&color=ff0179" width="500" height="281" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>[/sm_smlts_tooltip]
```

### Tabs

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#tabs)

Example shortcode

``` html
[sm_smlts_tabs type="lizatom-tabs-vertical"]
[sm_smlts_tab title="Title 1"]Content 1[/sm_smlts_tab]
[sm_smlts_tab title="Title 2"]Content 2[/sm_smlts_tab]
[sm_smlts_tab title="Title 3"]Content 3[/sm_smlts_tab]
[/sm_smlts_tabs]
```

### Accordion

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#accordion)

Example shortcode

``` html
[sm_smlts_accordions]
[sm_smlts_accordion title="Title 1"]Content 1[/sm_smlts_accordion]
[sm_smlts_accordion title="Title 2"]Content 2[/sm_smlts_accordion]
[sm_smlts_accordion title="Title 3"]Content 3[/sm_smlts_accordion]
[/sm_smlts_accordions]
```

### Simple tabs

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#simple-tabs-and-spoilers)

Example shortcode

``` html
[sm_smlts_simpletabs style="2"]
[sm_smlts_simpletab title="Tab name 1"] Tab content 1 [/sm_smlts_simpletab]
[sm_smlts_simpletab title="Tab name 2"] Tab content 2 [/sm_smlts_simpletab]
[sm_smlts_simpletab title="Tab name 3"] Tab content 3 [/sm_smlts_simpletab]
[/sm_smlts_simpletabs]
```

### Spoilers

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#simple-tabs-and-spoilers)

Example shortcode

``` html
[sm_smlts_spoiler title="Spoiler title"]Spoiler content[/sm_smlts_spoiler]
```

### Frames

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#frames-with-shadows)

Image

``` html
[sm_smlts_frame align="none" title="image title" alt="image alt"]https://lizatom.com/demo/lizatom-shortcodes/wp-content/uploads/2013/04/shutterstock_79886176-500x250.jpg[/sm_smlts_frame]
```

Google maps

``` html
[sm_smlts_frame align="none" title="image title" alt="image alt"]<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.cz/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=prague&amp;aq=&amp;sll=50.066396,14.465561&amp;sspn=0.338502,0.609055&amp;brcurrent=5,0,0&amp;ie=UTF8&amp;hq=&amp;hnear=Prague&amp;ll=50.066396,14.465561&amp;spn=0.021153,0.038066&amp;t=h&amp;z=14&amp;output=embed"></iframe>[/sm_smlts_frame]
```

### Info boxes

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#info-boxes)

Example shortcode

``` html
[sm_smlts_infobox type="image: info" clickable="yes" boldtext="Title"]Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut.[/sm_smlts_infobox]
```

### Color Boxes

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#color-boxes)

Example shortcode

``` html
[sm_smlts_colorbox title="Box title" color="#802323"]Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut.[/sm_smlts_colorbox]
```

### Notes

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#notes)

Example shortcode

``` html
[sm_smlts_note color="#f000ff"]Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut.[/sm_smlts_note]
```

### Pricing Table

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#pricing-table)

Example shortcode

``` html
[sm_smlts_pricing_table]
<!-- col 1 --><div class="lizatom-table-col">
<div class="lizatom-table-col-name">
Basic
</div>
<div class="lizatom-table-col-price">
<sup>$</sup>10<span class="sub">/MO</span>
</div>
<ul class="lizatom-table-col-items">
<li class="yes">5 Websites Cap</li>
<li class="no">Domain Name Included</li>
<li><a class="fancybutton fancysimple fancydefault fancylarge" href="#"><span class="cart">Add To Cart Now!</span></a></li>
</ul>
</div><!-- col 2 -->      
<div class="lizatom-table-col">
<div class="lizatom-table-col-name">
Premium
</div>
<div class="lizatom-table-col-price">
<sup>$</sup>12<span class="sub">/MO</span>
</div>
<ul class="lizatom-table-col-items">
<li class="yes">5 Websites Cap</li>
<li class="yes">Domain Name Included</li>
<li><a class="fancybutton fancysimple fancydefault fancylarge" href="#"><span class="cart">Add To Cart Now!</span></a></li>
</ul>
</div>
<div class="clear"></div>
[/sm_smlts_pricing_table]
```

Sometimes the 2nd column is displayed a bit lower than the first one. To fix this, please remove HTML comments from the
code.
```html
<!-- col 2 -->
```

### Blockquote

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#blockquote)

Example shortcode

``` html
[sm_smlts_blockquote align="left" cite="~John Doe, OurCorp, Ltd"]Vivamus maximus feugiat augue, ut interdum nisi facilisis et. Maecenas luctus tempus justo et dapibus. Sed tellus ipsum, sagittis vitae nulla sed, euismod porta ligula.[/sm_smlts_blockquote]
```

### Highlighters

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#highlighters)

Example shortcode

``` html
[sm_smlts_highlighter color="#409e19" text_color="#ffffff"]highlighted text[/sm_smlts_highlighter]
```

### Dropcaps

[Demo >](https://lizatom.com/demo/lizatom-shortcodes/#dropcaps)

Example shortcode

``` html
[sm_smlts_dropcap type="rounded-with-ring" color="gray"]D[/sm_smlts_dropcap]
```

Changelog
---------

1.0.6

* fancybuttons icons fix

1.0.5

* accordions fix

1.0.4

* list bullet fix

1.0.3

* columns shortcodes added to generator

1.0.2

* infobox fix

1.0.1

* columns shortcodes support (not via generator)

1.0.0

* Initial release

