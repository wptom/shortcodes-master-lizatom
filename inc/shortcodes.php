<?php
class Shortcodes_Master_Smlts_Shortcodes
    {
    
    static $globsimpletab = array();
	static $tab_count = 0;
        
    function __construct() { }

/**
 * 
 * 3d buttons
 */
    public static function smlts_3dbutton($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
             'id' => 'unique-lt3dbutton-id',
             'url' => 'http://mysite.com',
				'color' => 'gray',
				'icon' => 'basket',
				'target' => '_self',
				'animation' => '1',
				'float' => 'left'
            ), $atts, 'smlts');

        sm_query_asset( 'css', 'smlts' );
$threedeebutton_icon_path = plugins_url( '/assets/css/3dbuttons/' . $atts['icon'] . '.png',SMLTS_PLUGIN_FILE);
//$threedeebutton_icon_path = lts_plugin_url() . '/css/3dbuttons/' . $atts['icon'] . '.png';
		return '<div class="lizatom-3d-buttons-btn_container" style="float: ' . $atts['float'] . '"><a class="lizatom-3d-buttons-' . $atts['color'] . ' lizatom-3d-buttons-button" href="' . $atts['url'] . '" target="' . $atts['target'] . '"><span class="lizatom-3d-buttons-button_txt">'.$content.'</span><span class="lizatom-3d-buttons-a-btn-icon-right"><span class="lizatom-3d-buttons-right_icon' . $atts['animation'] . '"><img src="'.$threedeebutton_icon_path.'" /></span></span></a></div>';


        return $return;
        }

/**
 * 
 * fancy buttons
 */
    public static function smlts_fancybutton($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
             'id' => 'unique-ltfancybutton-id',
             'url' => '#',
				'color' => 'default',
				'size' => 'small',
				'icon' => 'heart',
				'target' => '_self',
				'display_icon' => 'yes'
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

		$button_icon = str_replace('image: ', '', $atts['icon']);
 if ($atts['display_icon']=="yes") { $iconwrapper_start = '<span class="' .$button_icon . '">'; } else { $iconwrapper_start = ''; }
                if ($atts['display_icon']=="yes") { $iconwrapper_end = '</span>'; } else { $iconwrapper_end = ''; }

		return 
		'<a class="fancybutton fancysimple fancy' . $atts['color'] . ' fancy' . $atts['size'] . '" href="' . $atts['url'] . '" target="' . $atts['target'] . '">' . 
        $iconwrapper_start .
        $content . 
        $iconwrapper_end .
        '</a>';

        return $return;
        }	

/**
 * 
 * list
 */
    public static function smlts_list($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            	'icon' => 'dot',
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );
        $list_icon = str_replace('image: ', '', $atts['icon']);
		
 $return .= '<ul class="lizatom-list '.$list_icon.'">';
	    $return .= do_shortcode($content);    
        $return .= '</ul>'; 

        return $return;
        }	
/**
 * 
 * tooltip
 */
    public static function smlts_tooltip($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            	'position' => 'top',
				'color' => 'blue',
				'delay' => '0',
                'text' => 'move mouse over me',
                'tooltiphead' => 'Tooltip head'
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

		
  if($atts['tooltiphead']) { $atts['tooltiphead'] = '<strong>' . $atts['tooltiphead'] . '</strong>'; } else { $atts['tooltiphead'] = ''; }

		return 
        '<a class="lizatom-tooltip-'.$atts['position'].' lizatom-tooltip-'.$atts['color'].' delay-'.$atts['color'].'" href="#"><span>'.
        $atts['tooltiphead'].
        do_shortcode($content).
        '</span>'.
        $atts['text'].
        '</a>'; 

        return $return;
		}	

/**
 * 
 * frame
 */
    public static function smlts_frame($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            	'type' => '',
				'align' => '',
				'title' => '',
				'alt' => ''
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

		
 //- If the content is an image add it to an img tag, else as is
        $strlen = strlen($content);
        $search = array('gif', 'jpg', 'jpeg','png');

        foreach($search as $srch_string) {
            $srch_string = '.'. $srch_string;
            $srch_len    = strlen($srch_string);
            $strpos      = strripos($content, $srch_string);
            
            if ($strpos == $strlen - $srch_len) {
                $content = '<img src="'.$content.'" alt="'.$atts['alt'].'" title="'.$atts['title'].'" />';
                break;
            }
        }

		$return .= '<div class="lizatom-box-wrapper" style="float: '.$atts['align'].';">';
		$return .= '<div class="lizatom-drop-shadow lizatom-'.$atts['type'].'">';
		$return .= do_shortcode($content);
		$return .= '</div>';
	    $return .= '</div>';
    
    return $return;
		}

/**
 * 
 * infobox
 */
    public static function smlts_infobox($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            	'type' => 'success',
				'clickable' => 'yes',
				'boldtext' => ''
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );
        sm_query_asset( 'js', 'smlts' );


		
 $clickable_icon = '';
                if($atts['clickable']=="yes") { $clickable_icon='<span class="close-infobox"></span>'; } else { $clickable_icon = ''; }
                if($atts['boldtext']) {$atts['boldtext'] = '<strong>' . $atts['boldtext'] . '</strong>'; } else { $atts['boldtext'] = ''; }

		return 
        '<div class="lizatom-infobox '.$atts['type'].'">'.$clickable_icon.'<div class="text"><p>'.$atts['boldtext'].' '. do_shortcode($content) . '</p></div></div>';
        }	

/**
 * 
 * colorbox
 */
    public static function smlts_colorbox($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            	'color' => '#333',
				'title' => __( 'Box title', 'smlts' )
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

		
	$styles = array(
			'dark_color' => Shortcodes_Master_Smlts::lt_hex_shift( $atts['color'], 'darker', 20 ),
			'light_color' => Shortcodes_Master_Smlts::lt_hex_shift( $atts['color'], 'lighter', 60 ),
			'text_shadow' => Shortcodes_Master_Smlts::lt_hex_shift( $atts['color'], 'darker', 70 ),
		);

		return '<div class="lt-box" style="border:1px solid ' . $styles['dark_color'] . '"><div class="lt-box-title" style="background-color:' . $atts['color'] . ';border-top:1px solid ' . $styles['light_color'] . ';text-shadow:1px 1px 0 ' . $styles['text_shadow'] . '">' . $atts['title'] . '</div><div class="lt-box-content">' . do_shortcode( $content ) . '</div></div>';
        }		

/**
 * 
 * note
 */
    public static function smlts_note($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            	'color' => '#fc0',
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

		
$styles = array(
			'dark_color' => lt_hex_shift( $atts['color'], 'darker', 10 ),
			'light_color' => lt_hex_shift( $atts['color'], 'lighter', 20 ),
			'extra_light_color' => lt_hex_shift( $atts['color'], 'lighter', 80 ),
			'text_color' => lt_hex_shift( $atts['color'], 'darker', 70 )
		);

		return '<div class="lt-note" style="background-color:' . $styles['light_color'] . ';border:1px solid ' . $styles['dark_color'] . '"><div class="lt-note-shell" style="border:1px solid ' . $styles['extra_light_color'] . ';color:' . $styles['text_color'] . '">' . do_shortcode( $content ) . '</div></div>';
        }		

/**
 * 
 * blockquote
 */
    public static function smlts_blockquote($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            	'align' => 'none',
        		'cite' => '~John Doe, OurCorp, Ltd'
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

		if ($atts['cite']!='') { $our_cite = '<p><cite>' . $atts['cite']  . '</cite></p>'; }
		$output .= '<blockquote class="lizatom-blockquote align' . $atts['align'] .'">';
        $output .= '<p>' . $content .'</p>';  
        $output .= $our_cite;      
		$output .= '</blockquote>';

		return $output;
		}
/**
 * 
 * highlighter
 */
    public static function smlts_highlighter($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            		'color' => '#c0c0c0',
            		'text_color' => '#000000'
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

			$output .= '<span class="lizatom-highlighter" style="background-color: '.$atts['color'].'; color: '.$atts['text_color'].'">' . $content . '</span>'; 

		return $output;
        }	

/**
 * 
 * dropcap
 */
    public static function smlts_dropcap($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            		'type' => 'rounded',
				  	'color' => 'black'
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

			$output .= '<div class="lizatom-dropcap-' .$atts['type']. ' ' .$atts['color']. '">' . do_shortcode($content) . '</div>';
		  
		  return $output;
        }

/**
 * 
 * fatcow
 */
    public static function smlts_fatcow($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            		'title' => 'Title',
				  	'icon' => 'image: accept'
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

			$service_icon = str_replace('image: ', '', $atts['icon']);
			$service_icon_path = plugins_url( 'assets/images/services/fatcow/32x32/', SMLTS_PLUGIN_FILE ) . $service_icon . '.png';
		   

	    $output .= '<div class="lizatom-service-fatcow">';
		$output .= '<h3><img src="'.$service_icon_path.'" />' . $atts['title'] . '</h3>'; 
	    $output .= do_shortcode($content);    
        $output .= '</div>';
		  
		  return $output;
        }	

/**
 * 
 * primo
 */
    public static function smlts_primo($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            		'title' => 'Title',
					'icon' => 'image: star_full',
					'size' => '48x48'
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

			$service_icon = str_replace('image: ', '', $atts['icon']);
			$service_icon_path = plugins_url( 'assets/images/services/primo/' . $atts['size'] . '/', SMLTS_PLUGIN_FILE ) . $service_icon . '.png';
		   

	    $output .= '<div class="lizatom-service-primo size-' . $atts['size'] . '">';
		$output .= '<h3><img src="'.$service_icon_path.'" />' . $atts['title'] . '</h3>'; 
	    $output .= do_shortcode($content);    
        $output .= '</div>';
		  
		  return $output;
        }	

/**
 * 
 * tab
 */
    public static function smlts_tab($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            		'title' => 'Tab title'
            ), $atts, 'smlts');

			$output .= '<dt>' .$atts['title']. '</dt>';
			$output .= '<dd>';   
			$output .= do_shortcode($content);    
			$output .= '</dd>';
		  
		  return $output;
        }
/**
 * 
 * tabs
 */
    public static function smlts_tabs($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            		'type' => 'lizatom-tabs-vertical'
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );
		sm_query_asset( 'js', 'smlts' );

        $output .= '<dl class="'.$atts['type'].'">';
	    $output .= do_shortcode($content);    
		$output .= '<div style="clear:both;"></div></dl>';     
			
		  
		  return $output;
		}
/**
 * 
 * accordion
 */
    public static function smlts_accordion($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            		'title' => 'Accordion title'
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );
			$output .= '<dt>' .$atts['title']. '</dt>';
        $output .= '<dd>';   
	    $output .= do_shortcode($content);    
        $output .= '</dd>';
		  
		  return $output;
		}
/**
 * 
 * accordions
 */
    public static function smlts_accordions($atts = null, $content = null)
        {
        $atts=shortcode_atts(array(), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );
		sm_query_asset( 'js', 'smlts' );

        $output .= '<dl class="lizatom-accordion">';
	    $output .= do_shortcode($content);    
        $output .= '</dl>';     
			
		  
		  return $output;
		}	
/**
 * 
 * simpletab
 */
    public static function smlts_simpletab($atts = null, $content = null)
        {
        $atts=shortcode_atts(array('title' => 'Title %d'), $atts, 'smlts');

		$x=self::$tab_count;
		
		self::$globsimpletab[$x]=array
			(
				'title' => $atts['title'],
				'content' => $content
			);

		self::$tab_count++;		
		}	

/**
 * 
 * simpletabs
 */
    public static function smlts_simpletabs($atts = null, $content = null)
        {
        	$output = '';
        	$simplepanes = array();
        	
        	sm_query_asset( 'js', 'smlts' );
			sm_query_asset( 'css', 'smlts' );
        	
        	$atts=shortcode_atts(array
				(
            		'style' => '1'
            	), $atts, 'smlts');		
		
		do_shortcode($content);
		
		if (is_array(self::$globsimpletab))
			{
				
				foreach (self::$globsimpletab as $tab)
				{
					$tabs[] = '<span>' . $tab['title'] . '</span>';
					$simplepanes[] = '<div class="lizatom-simpletabs-pane">' . $tab['content'] . '</div>';
					
				}
				
				$output = '<div class="lizatom-simpletabs lizatom-simpletabs-style-' . $atts['style'] . '"><div class="lizatom-simpletabs-nav">' . implode( $tabs ) . '</div><div class="lizatom-simpletabs-panes">' . implode(  $simplepanes ) . '</div><div class="lt-spacer"></div></div>';
			
			}
			
			
		self::$globsimpletab = array();
		self::$tab_count=0;
		  
		  return $output;
			
		}
/**
 * 
 * spoiler
 */
    public static function smlts_spoiler($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            		'title' => 'Spoiler title',
            		'style' => '0'
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );
		sm_query_asset( 'js', 'smlts' );

		return '<div class="lt-spoiler lt-spoiler-open lt-spoiler-style-' . $atts['style'] . '"><div class="lt-spoiler-title">' . $atts['title'] . '</div><div class="lt-spoiler-content">' . do_shortcode( $content ) . '</div></div>';
		}
/**
 * 
 * css3accordion
 */
    public static function smlts_css3accordion($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            		'title' => 'Accordion title must be unique'
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

		$newtitle = preg_replace('/\s+/', '_', $atts['title']);

        $output .= '<li class="block" id="'.$newtitle.'"><a class="header" href="#'.$newtitle.'">'.$atts['title'].'</a><div class="arrow"></div><div>';         
	    $output .= do_shortcode($content);    
        $output .= '</div></li>';     
    
    return $output;
		}	
/**
 *                
 * css3accordions
 */
    public static function smlts_css3accordions($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
            		'type' => 'lizatom-css3accordion-horizontal',
					'color' => '#000000',
					'title' => 'Accordion title'
            ), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );
		sm_query_asset( 'js', 'smlts' );

        $output .= '<div class="'.$atts['type'].'"><div class="qlabs_accordion" style="background-color: '.$atts['color'].'"><ul class="qlabs_accordion"><li class="block_header">'.$atts['title'].'</li>';
	    $output .= do_shortcode($content);    
		$output .= '</ul></div></div>';    
    
    return $output;
		}	

/**
 * 
 * portfolio
 */
    public static function smlts_portfolio($atts = null, $content = null)
        {
  
	sm_query_asset( 'css', 'smlts' );
    $atts=shortcode_atts(array
			(
            		'id' => 'unique-id-of-portfolio',
					'animation' => 1,
					'columns' => 3,
					'target' => '_blank',
					'categories' => ''
            ), $atts, 'smlts');

	$id = $atts['id'];
	$animation = $atts['animation'];
	$columns = $atts['columns'];
	$target = $atts['target'];
	$categories = $atts['categories'];

	if ( ($animation == 3) && ($columns == 2) ) { $type3width = '50%'; }
	if ( ($animation == 3) && ($columns == 3) ) { $type3width = '33.33%'; }
	if ( ($animation == 3) && ($columns == 4) ) { $type3width = '25%'; }
	if ( ($animation == 3) && ($columns == 5) ) { $type3width = '20%'; }	
	
	// make array of the categories input
     $cat_arr = explode( ',', $categories );
        
	//css of buttons
     foreach ($cat_arr as $value) {  
          
     $cat_data = get_category($value);   
        
     $output .= '
     <style>
       #'.$id.' .lts-portfolio-type1-container input.lts-portfolio-type1-selector-category-all-'.$id.':checked ~ label.lts-portfolio-type1-label-category-all-'.$id.',
	   #'.$id.' .lts-portfolio-type1-container input.lts-portfolio-type1-selector-category-'.$cat_data->slug.':checked ~ label.lts-portfolio-type1-label-category-'.$cat_data->slug.',
	   #'.$id.' .lts-portfolio-type2-container input.lts-portfolio-type2-selector-category-all-'.$id.':checked ~ label.lts-portfolio-type2-label-category-all-'.$id.',
	   #'.$id.' .lts-portfolio-type2-container input.lts-portfolio-type2-selector-category-'.$cat_data->slug.':checked ~ label.lts-portfolio-type2-label-category-'.$cat_data->slug.',
	   #'.$id.' .lts-portfolio-type3-container input.lts-portfolio-type3-selector-category-all-'.$id.':checked ~ label.lts-portfolio-type3-label-category-all-'.$id.',
	   #'.$id.' .lts-portfolio-type3-container input.lts-portfolio-type3-selector-category-'.$cat_data->slug.':checked ~ label.lts-portfolio-type3-label-category-'.$cat_data->slug.' {
			background: #777;
			color: #ffffff;
			text-shadow: 0px 1px 1px rgba(255,255,255,0.3);
		}	
		#'.$id.' .lts-portfolio-type1-container input.lts-portfolio-type1-selector-category-'.$cat_data->slug.':checked ~ .lts-portfolio-type1-items li:not(.lts-portfolio-type1-item-category-'.$cat_data->slug.') span,
		#'.$id.' .lts-portfolio-type2-container input.lts-portfolio-type2-selector-category-'.$cat_data->slug.':checked ~ .lts-portfolio-type2-items li:not(.lts-portfolio-type2-item-category-'.$cat_data->slug.') span {
			display:none;
		}			
		/*portfolio 1*/	
		#'.$id.' .lts-portfolio-type1-container input.lts-portfolio-type1-selector-category-all-'.$id.':checked ~ .lts-portfolio-type1-items li,
		#'.$id.' .lts-portfolio-type1-container input.lts-portfolio-type1-selector-category-'.$cat_data->slug.':checked ~ .lts-portfolio-type1-items .lts-portfolio-type1-item-category-'.$cat_data->slug.' {
			opacity: 1;
		}
		#'.$id.' .lts-portfolio-type1-container input.lts-portfolio-type1-selector-category-'.$cat_data->slug.':checked ~ .lts-portfolio-type1-items li:not(.lts-portfolio-type1-item-category-'.$cat_data->slug.') {
			opacity: 0.1;
		}		
		/*portfolio 2*/
		#'.$id.' .lts-portfolio-type2-container input.lts-portfolio-type2-selector-category-all-'.$id.':checked ~ .lts-portfolio-type2-items li,		
		#'.$id.' .lts-portfolio-type2-container input.lts-portfolio-type2-selector-category-'.$cat_data->slug.':checked ~ .lts-portfolio-type2-items .lts-portfolio-type2-item-'.$cat_data->slug.' {	
			opacity: 1;
			-webkit-transform: scale(1.1);
			-moz-transform: scale(1.1);
			-o-transform: scale(1.1);
			-ms-transform: scale(1.1);
			transform: scale(1.1);
		}
		#'.$id.' .lts-portfolio-type2-container input.lts-portfolio-type2-selector-category-'.$cat_data->slug.':checked ~ .lts-portfolio-type2-items li:not(.lts-portfolio-type2-item-category-'.$cat_data->slug.') {
			opacity: 0.1;
			-webkit-transform: scale(0.5);
			-moz-transform: scale(0.5);
			-o-transform: scale(0.5);
			-ms-transform: scale(0.5);
			transform: scale(0.5);
		}
		/*portfolio 3*/	
		#'.$id.' .lts-portfolio-type3-container input.lts-portfolio-type3-selector-category-all-'.$id.':checked ~ .lts-portfolio-type3-items li {	
			width: '.$type3width.';
			margin-bottom: 0px;			
			-webkit-transform: scale(1,1);
			-moz-transform: scale(1,1);
			-o-transform: scale(1,1);
			-ms-transform: scale(1,1);
			transform: scale(1,1);
			-webkit-transition: -webkit-transform 0.3s linear;
			-o-transition: -o-transform 0.3s linear;
			-ms-transition: -ms-transform 0.3s linear;
			transition: transform 0.3s linear;
		}
		#'.$id.' .lts-portfolio-type3-container input.lts-portfolio-type3-selector-category-'.$cat_data->slug.':checked ~ .lts-portfolio-type3-items .lts-portfolio-type3-item-category-'.$cat_data->slug.' {	
		margin-bottom: 0px;	       
				-webkit-transition: -webkit-transform 0.3s linear, width 0s linear 0.3s;
				-moz-transition: -moz-transform 0.3s linear, width 0s linear 0.3s;
				-o-transition: -o-transform 0.3s linear, width 0s linear 0.3s;
				-ms-transition: -ms-transform 0.3s linear, width 0s linear 0.3s;
				transition: transform 0.3s linear, width 0s linear 0.3s;
				-webkit-animation: scaleUp 0.3s linear 0.4s forwards;
				-moz-animation: scaleUp 0.3s linear 0.4s forwards;
				-o-animation: scaleUp 0.3s linear 0.4s forwards;
				-ms-animation: scaleUp 0.3s linear 0.4s forwards;
				animation: scaleUp 0.3s linear 0.4s forwards;
		}
		#'.$id.' .lts-portfolio-type3-container input.lts-portfolio-type3-selector-category-'.$cat_data->slug.':checked ~ .lts-portfolio-type3-items li:not(.lts-portfolio-type3-item-category-'.$cat_data->slug.')
		{
				-webkit-animation: scaleDown 0.3s linear forwards;
				-moz-animation: scaleDown 0.3s linear forwards;
				-o-animation: scaleDown 0.3s linear forwards;
				-ms-animation: scaleDown 0.3s linear forwards;
				animation: scaleDown 0.3s linear forwards;	
				width: 0; margin-left: 0 !important; margin-right: 0 !important;					
			}
			@-webkit-keyframes scaleUp {
				50% { width: '.$type3width.'; -webkit-transform: scale(0,0); }
			    100% { width: '.$type3width.'; -webkit-transform: scale(1,1); }
			}
			@-webkit-keyframes scaleDown {
				0% { width: '.$type3width.';-webkit-transform: scale(1,1);}
				99% { width: '.$type3width.'; -webkit-transform: scale(0,0);}
			    100% { width: 0; height: 0; -webkit-transform: scale(0,0); }
			}
			@-moz-keyframes scaleUp {
				50% { width: '.$type3width.'; -moz-transform: scale(0,0); }
			    100% { width: '.$type3width.'; -moz-transform: scale(1,1); }
			}
			@-moz-keyframes scaleDown {
				0% { width: '.$type3width.';-moz-transform: scale(1,1);}
				99% { width: '.$type3width.'; -moz-transform: scale(0,0);}
			    100% { width: 0; height: 0; -moz-transform: scale(0,0); }
			}
			@-o-keyframes scaleUp {
				50% { width: '.$type3width.'; -o-transform: scale(0,0); }
			    100% { width: '.$type3width.'; -o-transform: scale(1,1); }
			}
			@-o-keyframes scaleDown {
				0% { width: '.$type3width.';-o-transform: scale(1,1);}
				99% { width: '.$type3width.'; -o-transform: scale(0,0);}
			    100% { width: 0; height: 0; -o-transform: scale(0,0); }
			}
			@-ms-keyframes scaleUp {
				50% { width: '.$type3width.'; -ms-transform: scale(0,0); }
			    100% { width: '.$type3width.'; -ms-transform: scale(1,1); }
			}
			@-ms-keyframes scaleDown {
				0% { width: '.$type3width.';-ms-transform: scale(1,1);}
				99% { width: '.$type3width.'; -ms-transform: scale(0,0);}
			    100% { width: 0; height: 0; -ms-transform: scale(0,0); }
			}
			@keyframes scaleUp {
				50% { width: '.$type3width.'; transform: scale(0,0); }
			    100% { width: '.$type3width.'; transform: scale(1,1); }
			}
			@keyframes scaleDown {
				0% { width: '.$type3width.'; transform: scale(1,1);}
				99% { width: '.$type3width.'; transform: scale(0,0);}
			    100% { transform: scale(0,0); }
			}
					</style>
		';
     }	 
	 
	 $output .='<div id="'.$id.'">';
	 
     $output .= '<div class="lts-portfolio-type'.$animation.'-container">'; 
           
     //buttons          
     $output .= '<input id="select-category-all-'.$id.'" name="radio-set-'.$animation.'" type="radio" class="lts-portfolio-type'.$animation.'-selector-category-all-'.$id.'" checked="checked" /><label for="select-category-all-'.$id.'" class="lts-portfolio-type'.$animation.'-label-category-all-'.$id.'">All</label>';
     
     foreach ($cat_arr as $value) {       
     $cat_data = get_category($value);      
     $output .= '<input id="select-category-'.$cat_data->slug.'" name="radio-set-'.$animation.'" type="radio" class="lts-portfolio-type'.$animation.'-selector-category-'.$cat_data->slug.'" /><label for="select-category-'.$cat_data->slug.'" class="lts-portfolio-type'.$animation.'-label-category-'.$cat_data->slug.'">'.$cat_data->name.'</label>';
     }
     $output .='<div class="clear"></div>';
     
     //list of items
     $output .='<ul class="lts-portfolio-type'.$animation.'-items">';
    
     query_posts( 'cat='.$categories );    
     while ( have_posts() ) : the_post();     
     if ( has_post_thumbnail()) {
         $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');
         $image = lt_aq_resize($large_image_url[0], 500, 250, true);
     }      
     
     $our_cat_data = get_category(the_category_ID(false));
     
     if ($animation !=3) {
	     $count++;
	     if ($columns == '2') { $col_class = 'lts-one-half'; if ($count == 2) { $col_last = 'lts-col-last'; $count = 0; } else { $col_last = ''; } }
	     if ($columns == '3') { $col_class = 'lts-one-third'; if ($count == 3) { $col_last = 'lts-col-last'; $count = 0; } else { $col_last = ''; } }
	     if ($columns == '4') { $col_class = 'lts-one-fourth'; if ($count == 4) { $col_last = 'lts-col-last'; $count = 0; } else { $col_last = ''; } }
	     if ($columns == '5') { $col_class = 'lts-one-fifth'; if ($count == 5) { $col_last = 'lts-col-last'; $count = 0; } else { $col_last = ''; } }
	 }
     $output .='
     <li class="'.$col_class.' '.$col_last.' lts-portfolio-type'.$animation.'-item-category-'.$our_cat_data->slug.'"><a href="'.get_permalink().'" target="'.$target.'"><span></span><img src="'.$image.'" /></a></li>';     
     endwhile;
     wp_reset_query();
     $output .='<div class="clear"></div></ul>';
     $output .= '</div>';
     $output .='</div>';
     $output .= '<div class="clear"></div>';  
     
    
	 return $output;   	
		}
/**
 * 
 * pricing_table
 */
    public static function smlts_pricing_table($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

		$output .= '<div class="lizatom-table">';
        $output .= $content;        
	    $output .= '</div>';
    
    return $output;
		}
		
/**
 * 
 * cols
 */
    public static function smlts_cols($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

	    $output .= '<div class="lts-col-wrapper">';
	    $output .= do_shortcode($content);    
        $output .= '</div>';
		  
		  return $output;
        }		
/**
 * 
 * col
 */
    public static function smlts_col($atts = null, $content = null)
        {
        $atts=shortcode_atts(array
			(
				'type' => 'lts-one-half'
			), $atts, 'smlts');

		sm_query_asset( 'css', 'smlts' );

	    $output .= '<div class="' .$atts['type']. '">';
	    $output .= do_shortcode($content);    
        $output .= '</div>';
		  
		  return $output;
        }

	}
